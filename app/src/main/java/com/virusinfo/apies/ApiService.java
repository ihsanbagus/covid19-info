package com.virusinfo.apies;

import com.virusinfo.models.CountryModel;
import com.virusinfo.models.JabarModel;
import com.virusinfo.models.ProvinceModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("/")
    Call<List<CountryModel>> getCountry();

    @GET("/{country}/provinsi")
    Call<List<ProvinceModel>> getProvince(@Path("country") String country);

    @GET("aggregation.json")
    Call<List<JabarModel>> getJabar();
}