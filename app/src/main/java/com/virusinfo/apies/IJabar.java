package com.virusinfo.apies;

import com.virusinfo.models.JabarModel;

import java.util.List;

public interface IJabar {

    interface loadData {
        void onLoadDataJabarSuccess(List<JabarModel> data);

        void onLoadDataJabarFailure(String pesan);
    }
}
