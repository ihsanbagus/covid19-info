package com.virusinfo.apies;

import com.virusinfo.models.ProvinceModel;

import java.util.List;

public interface IProvince {

    interface loadData {
        void onLoadDataProvinceSuccess(List<ProvinceModel> data);

        void onLoadDataProvinceFailure(String pesan);
    }
}
