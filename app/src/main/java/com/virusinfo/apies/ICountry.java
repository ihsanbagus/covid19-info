package com.virusinfo.apies;

import com.virusinfo.models.CountryModel;

import java.util.List;

public interface ICountry {

    interface loadData {
        void onLoadDataCountrySuccess(List<CountryModel> data);

        void onLoadDataCountryFailure(String pesan);
    }
}
