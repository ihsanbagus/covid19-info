package com.virusinfo.models;

import com.google.gson.annotations.SerializedName;

public class JabarModel {

    @SerializedName("whatsapp")
    private String whatsapp;

    @SerializedName("total_positif_saat_ini")
    private int totalPositifSaatIni;

    @SerializedName("proses_pengawasan")
    private String prosesPengawasan;

    @SerializedName("sembuh")
    private String sembuh;

    @SerializedName("telepon")
    private String telepon;

    @SerializedName("proses_pemantauan")
    private String prosesPemantauan;

    @SerializedName("selesai_pemantauan")
    private String selesaiPemantauan;

    @SerializedName("meninggal")
    private String meninggal;

    @SerializedName("positif")
    private String positif;

    @SerializedName("total_sembuh")
    private int totalSembuh;

    @SerializedName("tanggal")
    private String tanggal;

    @SerializedName("total_odp")
    private String totalOdp;

    @SerializedName("total_pdp")
    private String totalPdp;

    @SerializedName("selesai_pengawasan")
    private String selesaiPengawasan;

    @SerializedName("total_meninggal")
    private int totalMeninggal;

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }

    public int getTotalPositifSaatIni() {
        return totalPositifSaatIni;
    }

    public void setTotalPositifSaatIni(int totalPositifSaatIni) {
        this.totalPositifSaatIni = totalPositifSaatIni;
    }

    public String getProsesPengawasan() {
        return prosesPengawasan;
    }

    public void setProsesPengawasan(String prosesPengawasan) {
        this.prosesPengawasan = prosesPengawasan;
    }

    public String getSembuh() {
        return sembuh;
    }

    public void setSembuh(String sembuh) {
        this.sembuh = sembuh;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getProsesPemantauan() {
        return prosesPemantauan;
    }

    public void setProsesPemantauan(String prosesPemantauan) {
        this.prosesPemantauan = prosesPemantauan;
    }

    public String getSelesaiPemantauan() {
        return selesaiPemantauan;
    }

    public void setSelesaiPemantauan(String selesaiPemantauan) {
        this.selesaiPemantauan = selesaiPemantauan;
    }

    public String getMeninggal() {
        return meninggal;
    }

    public void setMeninggal(String meninggal) {
        this.meninggal = meninggal;
    }

    public String getPositif() {
        return positif;
    }

    public void setPositif(String positif) {
        this.positif = positif;
    }

    public int getTotalSembuh() {
        return totalSembuh;
    }

    public void setTotalSembuh(int totalSembuh) {
        this.totalSembuh = totalSembuh;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTotalOdp() {
        return totalOdp;
    }

    public void setTotalOdp(String totalOdp) {
        this.totalOdp = totalOdp;
    }

    public String getTotalPdp() {
        return totalPdp;
    }

    public void setTotalPdp(String totalPdp) {
        this.totalPdp = totalPdp;
    }

    public String getSelesaiPengawasan() {
        return selesaiPengawasan;
    }

    public void setSelesaiPengawasan(String selesaiPengawasan) {
        this.selesaiPengawasan = selesaiPengawasan;
    }

    public int getTotalMeninggal() {
        return totalMeninggal;
    }

    public void setTotalMeninggal(int totalMeninggal) {
        this.totalMeninggal = totalMeninggal;
    }
}