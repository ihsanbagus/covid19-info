package com.virusinfo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReturnModel<O> implements Serializable {

    @SerializedName("status")
    private boolean status = false;

    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private O data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public O getData() {
        return data;
    }

    public void setData(O data) {
        this.data = data;
    }
}