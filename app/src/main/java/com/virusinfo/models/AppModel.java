package com.virusinfo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppModel implements Serializable {

    @SerializedName("uri_api")
    private String uri_api;

    @SerializedName("jabar_api")
    private String jabar_api;

    @SerializedName("download_uri")
    private String download_uri;

    @SerializedName("running_text")
    private String running_text;

    @SerializedName("version")
    private String version;

    @SerializedName("show_ad")
    private boolean show_ad;

    @SerializedName("ads_id")
    private String ads_id;

    @SerializedName("ban_id")
    private String ban_id;

    @SerializedName("int_id")
    private String int_id;

    @SerializedName("startapp")
    private String startapp;

    public String getUri_api() {
        return uri_api;
    }

    public void setUri_api(String uri_api) {
        this.uri_api = uri_api;
    }

    public String getJabar_api() {
        return jabar_api;
    }

    public void setJabar_api(String jabar_api) {
        this.jabar_api = jabar_api;
    }

    public String getDownload_uri() {
        return download_uri;
    }

    public void setDownload_uri(String download_uri) {
        this.download_uri = download_uri;
    }

    public String getRunning_text() {
        return running_text;
    }

    public void setRunning_text(String running_text) {
        this.running_text = running_text;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isShow_ad() {
        return show_ad;
    }

    public void setShow_ad(boolean show_ad) {
        this.show_ad = show_ad;
    }

    public String getAds_id() {
        return ads_id;
    }

    public void setAds_id(String ads_id) {
        this.ads_id = ads_id;
    }

    public String getBan_id() {
        return ban_id;
    }

    public void setBan_id(String ban_id) {
        this.ban_id = ban_id;
    }

    public String getInt_id() {
        return int_id;
    }

    public void setInt_id(String int_id) {
        this.int_id = int_id;
    }

    public String getStartapp() {
        return startapp;
    }

    public void setStartapp(String startapp) {
        this.startapp = startapp;
    }
}