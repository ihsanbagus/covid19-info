package com.virusinfo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AdsModel implements Serializable {

    @SerializedName("ads_id")
    private String ads_id;

    @SerializedName("ban_id")
    private String ban_id;

    @SerializedName("int_id")
    private String int_id;

    public String getAds_id() {
        return ads_id;
    }

    public void setAds_id(String ads_id) {
        this.ads_id = ads_id;
    }

    public String getBan_id() {
        return ban_id;
    }

    public void setBan_id(String ban_id) {
        this.ban_id = ban_id;
    }

    public String getInt_id() {
        return int_id;
    }

    public void setInt_id(String int_id) {
        this.int_id = int_id;
    }
}
