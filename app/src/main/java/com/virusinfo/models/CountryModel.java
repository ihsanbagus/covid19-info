package com.virusinfo.models;

import com.google.gson.annotations.SerializedName;

public class CountryModel {

    @SerializedName("attributes")
    private Attributes attributes;

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public static class Attributes {

        @SerializedName("OBJECTID")
        private int objectId;

        @SerializedName("Long_")
        private double long_;

        @SerializedName("Recovered")
        private int recovered;

        @SerializedName("Country_Region")
        private String countryRegion;

        @SerializedName("Active")
        private int active;

        @SerializedName("Last_Update")
        private long lastUpdate;

        @SerializedName("Deaths")
        private int deaths;

        @SerializedName("Confirmed")
        private int confirmed;

        @SerializedName("Lat")
        private double lat;

        public void setOBJECTID(int objectId) {
            this.objectId = objectId;
        }

        public int getOBJECTID() {
            return objectId;
        }

        public void setLong(double long_) {
            this.long_ = long_;
        }

        public double getLong() {
            return long_;
        }

        public void setRecovered(int recovered) {
            this.recovered = recovered;
        }

        public int getRecovered() {
            return recovered;
        }

        public void setCountryRegion(String countryRegion) {
            this.countryRegion = countryRegion;
        }

        public String getCountryRegion() {
            return countryRegion;
        }

        public void setActive(int active) {
            this.active = active;
        }

        public int getActive() {
            return active;
        }

        public void setLastUpdate(long lastUpdate) {
            this.lastUpdate = lastUpdate;
        }

        public long getLastUpdate() {
            return lastUpdate;
        }

        public void setDeaths(int deaths) {
            this.deaths = deaths;
        }

        public int getDeaths() {
            return deaths;
        }

        public void setConfirmed(int confirmed) {
            this.confirmed = confirmed;
        }

        public int getConfirmed() {
            return confirmed;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLat() {
            return lat;
        }

    }
}