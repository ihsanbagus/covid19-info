package com.virusinfo.datas;

import androidx.annotation.NonNull;

import com.virusinfo.apies.ApiClient;
import com.virusinfo.apies.ApiService;
import com.virusinfo.apies.IJabar;
import com.virusinfo.models.JabarModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JabarData {
    private IJabar.loadData iDatas;

    public JabarData(IJabar.loadData iDatas) {
        this.iDatas = iDatas;
    }

    public void getJabar(String uri) {
        Call<List<JabarModel>> call = ApiClient.getClient(uri).create(ApiService.class).getJabar();
        call.enqueue(new Callback<List<JabarModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<JabarModel>> call, @NonNull Response<List<JabarModel>> response) {
                try {
                    if (!response.body().isEmpty()) {
                        List<JabarModel> data = response.body();
                        iDatas.onLoadDataJabarSuccess(data);
                    } else {
                        iDatas.onLoadDataJabarFailure(response.message());
                    }
                } catch (Exception e) {
                    iDatas.onLoadDataJabarFailure(e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<JabarModel>> call, @NonNull Throwable t) {
                iDatas.onLoadDataJabarFailure(t.getMessage());
                call.cancel();
            }
        });
    }
}
