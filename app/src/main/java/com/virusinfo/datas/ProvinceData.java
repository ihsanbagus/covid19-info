package com.virusinfo.datas;

import androidx.annotation.NonNull;

import com.virusinfo.apies.ApiClient;
import com.virusinfo.apies.ApiService;
import com.virusinfo.apies.IProvince;
import com.virusinfo.models.ProvinceModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProvinceData {
    private IProvince.loadData iDatas;

    public ProvinceData(IProvince.loadData iDatas) {
        this.iDatas = iDatas;
    }

    public void getProvince(String uri, String country) {
        Call<List<ProvinceModel>> call = ApiClient.getClient(uri).create(ApiService.class).getProvince(country);
        call.enqueue(new Callback<List<ProvinceModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<ProvinceModel>> call, @NonNull Response<List<ProvinceModel>> response) {
                try {
                    if (!response.body().isEmpty()) {
                        List<ProvinceModel> data = response.body();
                        iDatas.onLoadDataProvinceSuccess(data);
                    } else {
                        iDatas.onLoadDataProvinceFailure(response.message());
                    }
                } catch (Exception e) {
                    iDatas.onLoadDataProvinceFailure(e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<ProvinceModel>> call, @NonNull Throwable t) {
                iDatas.onLoadDataProvinceFailure(t.getMessage());
                call.cancel();
            }
        });
    }
}
