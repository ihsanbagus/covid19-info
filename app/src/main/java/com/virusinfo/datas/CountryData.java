package com.virusinfo.datas;

import androidx.annotation.NonNull;

import com.virusinfo.apies.ApiClient;
import com.virusinfo.apies.ApiService;
import com.virusinfo.apies.ICountry;
import com.virusinfo.models.CountryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryData {
    private ICountry.loadData iDatas;

    public CountryData(ICountry.loadData iDatas) {
        this.iDatas = iDatas;
    }

    public void getCountry(String uri) {
        Call<List<CountryModel>> call = ApiClient.getClient(uri).create(ApiService.class).getCountry();
        call.enqueue(new Callback<List<CountryModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<CountryModel>> call, @NonNull Response<List<CountryModel>> response) {
                try {
                    if (!response.body().isEmpty()) {
                        List<CountryModel> data = response.body();
                        iDatas.onLoadDataCountrySuccess(data);
                    } else {
                        iDatas.onLoadDataCountryFailure(response.message());
                    }
                } catch (Exception e) {
                    iDatas.onLoadDataCountryFailure(e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<CountryModel>> call, @NonNull Throwable t) {
                iDatas.onLoadDataCountryFailure(t.getMessage());
                call.cancel();
            }
        });
    }
}
