package com.virusinfo.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import com.virusinfo.BuildConfig;
import com.virusinfo.R;
import com.virusinfo.adapters.CountryAdapter;
import com.virusinfo.apies.ICountry;
import com.virusinfo.datas.CountryData;
import com.virusinfo.models.AppModel;
import com.virusinfo.models.CountryModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.virusinfo.helpers.MyHelper.formatRibuan;
import static com.virusinfo.helpers.SPHelper.createSP;
import static com.virusinfo.helpers.SPHelper.readSP;

public class DashboardActivity extends BaseActivity implements ICountry.loadData {

    @BindView(R.id.iSearch)
    AppCompatAutoCompleteTextView iSearch;
    @BindView(R.id.loading)
    FrameLayout loading;
    @BindView(R.id.list_country)
    RecyclerView listCountry;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;
    @BindView(R.id.positive)
    AppCompatTextView positive;
    @BindView(R.id.main_content)
    LinearLayoutCompat mainContent;
    @BindView(R.id.parent)
    CoordinatorLayout parent;
    @BindView(R.id.recovered)
    AppCompatTextView recovered;
    @BindView(R.id.running_text)
    AppCompatTextView runningText;
    @BindView(R.id.death)
    AppCompatTextView death;
    @BindView(R.id.ads)
    LinearLayoutCompat ads;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private CountryData countryData = new CountryData(this);
    private CountryAdapter countryAdapter;
    private List<CountryModel> tmpData = new ArrayList<>();
    private String uri = "";
    private String versi = "1.0";
    private String newVersi = "1.0";
    private String ads_id;
    private String ban_id;
    private String int_id;
    private String download_uri = "";
    private int p = 0;
    private int r = 0;
    private int d = 0;
    private boolean isShow_ad = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        ads_id = getString(R.string.ads_id);
        ban_id = getString(R.string.ban_id);
        int_id = getString(R.string.int_id);
        initComponent();
    }

    void initComponent() {
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(getResources().getString(R.string.app_alias));
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot dataSnapshot) {
                AppModel appModel = dataSnapshot.getValue(AppModel.class);
                uri = Objects.requireNonNull(appModel).getUri_api();
                countryData.getCountry(uri);
                download_uri = appModel.getDownload_uri();
                runningText.setText(appModel.getRunning_text());
                newVersi = appModel.getVersion();
                isShow_ad = appModel.isShow_ad();
                createSP(DashboardActivity.this, "isShow_ad", isShow_ad);
                createSP(DashboardActivity.this, "download_uri", download_uri);
                createSP(DashboardActivity.this, "running_text", appModel.getRunning_text());
                createSP(DashboardActivity.this, "startapp", appModel.getStartapp());
                createSP(DashboardActivity.this, "ads_id", appModel.getAds_id() == null || appModel.getAds_id().length() != 38 ? ads_id : appModel.getAds_id());
                createSP(DashboardActivity.this, "ban_id", appModel.getBan_id() == null || appModel.getBan_id().length() != 38 ? ban_id : appModel.getBan_id());
                createSP(DashboardActivity.this, "int_id", appModel.getInt_id() == null || appModel.getInt_id().length() != 38 ? int_id : appModel.getInt_id());
                ads_id = (String) readSP(DashboardActivity.this, "ads_id", "string");
                ban_id = (String) readSP(DashboardActivity.this, "ban_id", "string");
                int_id = (String) readSP(DashboardActivity.this, "int_id", "string");
                if (isShow_ad) {
                    MobileAds.initialize(DashboardActivity.this, ads_id);
                    prepareBan();
                    StartAppSDK.init(DashboardActivity.this, appModel.getStartapp(), false);
                    StartAppAd.disableSplash();
                }
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {
                // Failed to read value
            }
        });

        runningText.setSelected(true);
        refresh.setRefreshing(true);
        countryAdapter = new CountryAdapter(this);
        listCountry.setAdapter(countryAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        listCountry.setLayoutManager(mLayoutManager);
        listCountry.setItemAnimator(new DefaultItemAnimator());
        listCountry.setAdapter(countryAdapter);

        iSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<CountryModel> cm = new ArrayList<>();
                for (CountryModel data : tmpData) {
                    if (s.toString().isEmpty()) {
                        cm = tmpData;
                    } else if (data.getAttributes().getCountryRegion().toLowerCase().contains(s.toString().toLowerCase())) {
                        cm.add(data);
                    }
                }
                countryAdapter.setData(cm);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        refresh.setOnRefreshListener(() -> {
            showInt();
            iSearch.setText("");
            loading.setVisibility(View.VISIBLE);
            countryData.getCountry(uri);
        });
    }

    private void prepareBan() {
        ads.removeAllViews();
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(ban_id);
        mAdView.loadAd(new AdRequest.Builder().build());
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                ads.removeAllViews();
                ads.addView(new Banner(DashboardActivity.this));
            }

            @Override
            public void onAdLoaded() {
                ads.removeAllViews();
                ads.addView(mAdView);
            }
        });
    }

    private void prepareInt() {
        createSP(this, "clickCounter", 0);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(int_id);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                StartAppAd.showAd(DashboardActivity.this);
            }

            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }
        });
    }

    public void showInt() {
        if (cc() >= 5 && isShow_ad) {
            prepareInt();
        }
    }

    private int cc() {
        int cc = (int) readSP(this, "clickCounter", 0);
        createSP(this, "clickCounter", ++cc);
        return cc;
    }

    @Override
    public void onLoadDataCountrySuccess(List<CountryModel> data) {
        p = 0;
        r = 0;
        d = 0;
        tmpData = data;
        countryAdapter.setData(data);
        for (CountryModel cm : data) {
            p += cm.getAttributes().getConfirmed();
            r += cm.getAttributes().getRecovered();
            d += cm.getAttributes().getDeaths();
        }
        positive.setText(new StringBuilder().append(formatRibuan(p)));
        recovered.setText(new StringBuilder().append(formatRibuan(r)));
        death.setText(new StringBuilder().append(formatRibuan(d)));
        refresh.setRefreshing(false);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void onLoadDataCountryFailure(String pesan) {
        Toast.makeText(this, pesan, Toast.LENGTH_SHORT).show();
        refresh.setRefreshing(false);
        loading.setVisibility(View.GONE);
    }

    private void showDialogAbout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_about);
        dialog.setCancelable(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((AppCompatTextView) dialog.findViewById(R.id.tv_version)).setText(String.format("Version %s", BuildConfig.VERSION_NAME));

        (dialog.findViewById(R.id.developer)).setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(download_uri));
            startActivity(i);
        });

        (dialog.findViewById(R.id.cek_update)).setOnClickListener(v -> {
            if (!versi.equalsIgnoreCase(newVersi)) {
                ((AppCompatTextView) dialog.findViewById(R.id.text_version)).setText(new StringBuilder().append("New version ").append(newVersi).append(" is available"));
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(download_uri));
                startActivity(i);
            } else {
                ((AppCompatTextView) dialog.findViewById(R.id.text_version)).setText(new StringBuilder().append("🥳 No Update"));
            }
        });

        (dialog.findViewById(R.id.bt_close)).setOnClickListener(v -> dialog.dismiss());
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @OnClick({R.id.share, R.id.about, R.id.global_case, R.id.indonesia})
    public void onViewClicked(View view) {
        showInt();
        switch (view.getId()) {
            case R.id.share:
                String shareBody = "Kasus Covid19 *Seluruh Dunia*"
                        + "\n\n\uD83D\uDE14 *Positive*: " + formatRibuan(p)
                        + "\n\uD83D\uDE07 *Recovered*: " + formatRibuan(r)
                        + "\n\uD83D\uDE2D *Death*: " + formatRibuan(d)
//                    + "\n\n\uD83D\uDD52 Last Update: " + new Date(lastUpdate)
                        + "\n\n⬇ Download Apps on : " + readSP(DashboardActivity.this, "download_uri", "string");
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Covid19");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share with :"));
                break;
            case R.id.about:
                showDialogAbout();
                break;
            case R.id.indonesia:
                Intent i = new Intent(this, CountryActivity.class);
                i.putExtra("uri", uri);
                i.putExtra("country", "Indonesia");
                startActivity(i);
                break;
        }
    }
}
