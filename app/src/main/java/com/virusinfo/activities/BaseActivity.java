package com.virusinfo.activities;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.virusinfo.R;

import java.util.Objects;

public class BaseActivity extends AppCompatActivity {

    void initToolbar(Toolbar toolbar) {
        toolbar.findViewById(R.id.title).setSelected(true);
        toolbar.findViewById(R.id.subTitle).setSelected(true);
//        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Toolbar");
//        getSupportActionBar().setSubtitle("wew");
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
//        Tools.setSystemBarColor(this);
    }

}
