package com.virusinfo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.FrameLayout;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import com.virusinfo.R;
import com.virusinfo.adapters.ProvinceAdapter;
import com.virusinfo.apies.IProvince;
import com.virusinfo.datas.ProvinceData;
import com.virusinfo.models.ProvinceModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.virusinfo.helpers.SPHelper.createSP;
import static com.virusinfo.helpers.SPHelper.readSP;

public class CountryActivity extends BaseActivity implements IProvince.loadData {

    @BindView(R.id.country_name)
    AppCompatTextView countryName;
    @BindView(R.id.positive)
    AppCompatTextView positive;
    @BindView(R.id.recovered)
    AppCompatTextView recovered;
    @BindView(R.id.death)
    AppCompatTextView death;
    @BindView(R.id.ads)
    LinearLayoutCompat ads;
    @BindView(R.id.running_text)
    AppCompatTextView runningText;
    @BindView(R.id.iSearch)
    AppCompatAutoCompleteTextView iSearch;
    @BindView(R.id.list_province)
    RecyclerView listProvince;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;
    @BindView(R.id.loading)
    FrameLayout loading;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private ProvinceAdapter provinceAdapter;
    private ProvinceData provinceData = new ProvinceData(this);
    private List<ProvinceModel> tmpData = new ArrayList<>();
    private String uri = "";
    private String country = "";
    private String ads_id;
    private String ban_id;
    private String int_id;
    private String startapp;
    private int p = 0;
    private int r = 0;
    private int d = 0;
    private boolean isShow_ad = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        ButterKnife.bind(this);
        isShow_ad = (boolean) readSP(this, "isShow_ad", false);
        startapp = (String) readSP(this, "startapp", "string");
        ads_id = (String) readSP(this, "ads_id", "string");
        ban_id = (String) readSP(this, "ban_id", "string");
        int_id = (String) readSP(this, "int_id", "string");
        initComponent();
    }

    private void initComponent() {
        uri = getIntent().getStringExtra("uri");
        country = getIntent().getStringExtra("country");
        countryName.setText(country);
        runningText.setText((String) readSP(this, "running_text", "string"));
        provinceData.getProvince(uri, country.toLowerCase());

        if (isShow_ad) {
            MobileAds.initialize(this, ads_id);
            prepareBan();
            StartAppSDK.init(this, startapp, false);
            StartAppAd.disableSplash();
        }

        runningText.setSelected(true);
        refresh.setRefreshing(true);
        provinceAdapter = new ProvinceAdapter(this);
        listProvince.setAdapter(provinceAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        listProvince.setLayoutManager(mLayoutManager);
        listProvince.setItemAnimator(new DefaultItemAnimator());
        listProvince.setAdapter(provinceAdapter);

        iSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<ProvinceModel> cm = new ArrayList<>();
                for (ProvinceModel data : tmpData) {
                    if (s.toString().isEmpty()) {
                        cm = tmpData;
                    } else if (data.getAttributes().getProvinsi().toLowerCase().contains(s.toString().toLowerCase())) {
                        cm.add(data);
                    }
                }
                provinceAdapter.setData(cm);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        refresh.setOnRefreshListener(() -> {
            showInt();
            iSearch.setText("");
            loading.setVisibility(View.VISIBLE);
            provinceData.getProvince(uri, country.toLowerCase());
        });

    }

    private void prepareBan() {
        ads.removeAllViews();
        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(ban_id);
        mAdView.loadAd(new AdRequest.Builder().build());
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                ads.removeAllViews();
                ads.addView(new Banner(CountryActivity.this));
            }

            @Override
            public void onAdLoaded() {
                ads.removeAllViews();
                ads.addView(mAdView);
            }
        });
    }

    private void prepareInt() {
        createSP(this, "clickCounter", 0);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(int_id);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                StartAppAd.showAd(CountryActivity.this);
            }

            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }
        });
    }

    public void showInt() {
        if (cc() >= 5 && isShow_ad) {
            prepareInt();
        }
    }

    private int cc() {
        int cc = (int) readSP(this, "clickCounter", 0);
        createSP(this, "clickCounter", ++cc);
        return cc;
    }

    @Override
    public void onLoadDataProvinceSuccess(List<ProvinceModel> data) {
        p = 0;
        r = 0;
        d = 0;
        tmpData = data;
        provinceAdapter.setData(tmpData);
        for (ProvinceModel cm : data) {
            p += cm.getAttributes().getKasusPosi();
            r += cm.getAttributes().getKasusSemb();
            d += cm.getAttributes().getKasusMeni();
        }
        positive.setText(new StringBuilder().append(p));
        recovered.setText(new StringBuilder().append(r));
        death.setText(new StringBuilder().append(d));
        refresh.setRefreshing(false);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void onLoadDataProvinceFailure(String pesan) {
        refresh.setRefreshing(false);
        loading.setVisibility(View.GONE);
    }

    @OnClick({R.id.share, R.id.card_statistics, R.id.iSearch})
    public void onViewClicked(View view) {
        showInt();
        if (view.getId() == R.id.share) {
            String shareBody = "Covid19 data from *"
                    + country
                    + "*\n\n\uD83D\uDE14 *Positive*: " + p
                    + "\n\uD83D\uDE07 *Recovered*: " + r
                    + "\n\uD83D\uDE2D *Death*: " + d
//                    + "\n\n\uD83D\uDD52 Last Update: " + new Date(lastUpdate)
                    + "\n\n⬇ Download Apps on : " + readSP(CountryActivity.this, "download_uri", "string");
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, country);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share with :"));
        }
    }
}
