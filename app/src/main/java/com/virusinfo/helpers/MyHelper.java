package com.virusinfo.helpers;

import java.text.NumberFormat;
import java.util.Locale;

public class MyHelper {

    public static String ucword(String str) {
        String[] strArray = str.trim().replaceAll("\\s+", " ").toLowerCase().split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap).append(" ");
        }
        return builder.toString();
    }

    public static String formatRibuan(int angka) {
        String numberFormat = NumberFormat.getInstance(Locale.US).format(angka);
        return numberFormat.replaceAll(",", ".");
    }
}


