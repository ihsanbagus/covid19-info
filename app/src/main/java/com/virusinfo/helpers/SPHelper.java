package com.virusinfo.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SPHelper {

    public static void createSP(Activity act, String key, Object value) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        }
        if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        }
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        }
        if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        }
        if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        }
        editor.apply();
    }

    public static Object readSP(Activity act, String key, Object type) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        Object o = null;
        if (type instanceof String) {
            o = mSettings.getString(key, "");
        }
        if (type instanceof Integer) {
            o = mSettings.getInt(key, 0);
        }
        if (type instanceof Boolean) {
            o = mSettings.getBoolean(key, false);
        }
        if (type instanceof Float) {
            o = mSettings.getFloat(key, 0);
        }
        if (type instanceof Long) {
            o = mSettings.getLong(key, 0);
        }
        return o;
    }

    public static void deleteSP(Activity act, String key) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.remove(key);
        editor.apply();
    }

    public static void deleteAllSP(Activity act) {
        SharedPreferences mSettings = act.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.clear();
        editor.apply();
    }

}
