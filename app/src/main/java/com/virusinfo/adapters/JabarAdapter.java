package com.virusinfo.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.virusinfo.R;
import com.virusinfo.activities.DashboardActivity;
import com.virusinfo.models.JabarModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JabarAdapter extends RecyclerView.Adapter<JabarAdapter.OriginalViewHolder> {

    private Activity act;
    private List<JabarModel> jabarModels;

    public JabarAdapter(Activity act) {
        this.act = act;
    }

    public void setData(List<JabarModel> jabarModels) {
        this.jabarModels = jabarModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OriginalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(act).inflate(R.layout.item_jabar, parent, false);
        return new OriginalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OriginalViewHolder holder, int position) {
        JabarModel jb = jabarModels.get(position);
        holder.tanggal.setText(jb.getTanggal());
        holder.positif.setText(TextUtils.isEmpty(jb.getPositif()) ? "0" : jb.getPositif());
        holder.sembuh.setText(TextUtils.isEmpty(jb.getSembuh()) ? "0" : jb.getSembuh());
        holder.meninggal.setText(TextUtils.isEmpty(jb.getMeninggal()) ? "0" : jb.getMeninggal());
        holder.parent.setOnClickListener(v -> {
            ((DashboardActivity) act).showInt();
        });
    }

    @Override
    public int getItemCount() {
        return jabarModels == null ? 0 : jabarModels.size();
    }

    static class OriginalViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tanggal)
        AppCompatTextView tanggal;
        @BindView(R.id.positif)
        AppCompatTextView positif;
        @BindView(R.id.sembuh)
        AppCompatTextView sembuh;
        @BindView(R.id.meninggal)
        AppCompatTextView meninggal;
        @BindView(R.id.parent)
        CardView parent;

        OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
