package com.virusinfo.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.virusinfo.R;
import com.virusinfo.activities.DashboardActivity;
import com.virusinfo.fragments.FragmentBottomDetail;
import com.virusinfo.models.CountryModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.virusinfo.helpers.MyHelper.formatRibuan;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.OriginalViewHolder> {
    private Activity act;
    private List<CountryModel> countryModels;

    public CountryAdapter(Activity act) {
        this.act = act;
    }

    public void setData(List<CountryModel> countryModels) {
        this.countryModels = countryModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OriginalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(act).inflate(R.layout.item_list, parent, false);
        return new OriginalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OriginalViewHolder holder, int position) {
        CountryModel cd = countryModels.get(position);
        CountryModel.Attributes attr = cd.getAttributes();
        holder.countryName.setText(attr.getCountryRegion());
        int total = attr.getConfirmed() + attr.getDeaths() + attr.getRecovered();
        holder.caseTotal.setText(new StringBuilder().append(formatRibuan(total)));
        holder.parent.setOnClickListener(v -> {
            ((DashboardActivity) act).showInt();
            FragmentBottomDetail na = new FragmentBottomDetail(act, cd);
            na.show(((DashboardActivity) act).getSupportFragmentManager(), na.getTag());
        });
    }

    @Override
    public int getItemCount() {
        return countryModels == null ? 0 : countryModels.size();
    }

    static class OriginalViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.parent)
        MaterialRippleLayout parent;
        @BindView(R.id.item_name)
        AppCompatTextView countryName;
        @BindView(R.id.case_total)
        AppCompatTextView caseTotal;

        OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
