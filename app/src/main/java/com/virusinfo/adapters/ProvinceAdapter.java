package com.virusinfo.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.virusinfo.R;
import com.virusinfo.activities.CountryActivity;
import com.virusinfo.fragments.FragmentBottomDetail;
import com.virusinfo.models.ProvinceModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.virusinfo.helpers.MyHelper.formatRibuan;

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.OriginalViewHolder> {
    private Activity act;
    private List<ProvinceModel> ProvinceModels;

    public ProvinceAdapter(Activity act) {
        this.act = act;
    }

    public void setData(List<ProvinceModel> ProvinceModels) {
        this.ProvinceModels = ProvinceModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OriginalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(act).inflate(R.layout.item_list, parent, false);
        return new OriginalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OriginalViewHolder holder, int position) {
        ProvinceModel cd = ProvinceModels.get(position);
        ProvinceModel.Attributes attr = cd.getAttributes();
        holder.iconItem.setImageResource(R.drawable.province);
        holder.itemName.setText(attr.getProvinsi());
        int total = attr.getKasusPosi() + attr.getKasusSemb() + attr.getKasusMeni();
        holder.caseTotal.setText(new StringBuilder().append(formatRibuan(total)));
        holder.parent.setOnClickListener(v -> {
            ((CountryActivity) act).showInt();
            FragmentBottomDetail na = new FragmentBottomDetail(act, cd);
            na.show(((CountryActivity) act).getSupportFragmentManager(), na.getTag());
        });
    }

    @Override
    public int getItemCount() {
        return ProvinceModels == null ? 0 : ProvinceModels.size();
    }

    static class OriginalViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.parent)
        MaterialRippleLayout parent;
        @BindView(R.id.icon_item)
        AppCompatImageView iconItem;
        @BindView(R.id.item_name)
        AppCompatTextView itemName;
        @BindView(R.id.case_total)
        AppCompatTextView caseTotal;

        OriginalViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
