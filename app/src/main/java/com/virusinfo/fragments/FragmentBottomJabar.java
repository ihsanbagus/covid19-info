package com.virusinfo.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import com.virusinfo.R;
import com.virusinfo.activities.DashboardActivity;
import com.virusinfo.adapters.JabarAdapter;
import com.virusinfo.models.JabarModel;
import com.vivian.timelineitemdecoration.itemdecoration.DotItemDecoration;
import com.vivian.timelineitemdecoration.itemdecoration.SpanIndexListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.virusinfo.helpers.SPHelper.readSP;

public class FragmentBottomJabar extends BottomSheetDialogFragment {

    @BindView(R.id.title)
    AppCompatTextView title;
    @BindView(R.id.subTitle)
    AppCompatTextView subTitle;
    @BindView(R.id.bt_close)
    AppCompatImageButton btClose;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.lyt_parent)
    LinearLayoutCompat lytParent;
    @BindView(R.id.ads)
    LinearLayoutCompat ads_layout;
    @BindView(R.id.odp)
    AppCompatTextView odp;
    @BindView(R.id.pdp)
    AppCompatTextView pdp;
    @BindView(R.id.positive)
    AppCompatTextView positive;
    //    @BindView(R.id.lyt_spacer)
//    View lytSpacer;
    @BindView(R.id.bt_share)
    AppCompatImageButton btShare;
    @BindView(R.id.recovered)
    AppCompatTextView recovered;
    @BindView(R.id.death)
    AppCompatTextView death;
    @BindView(R.id.update_data)
    RecyclerView updateData;
    @BindView(R.id.card_statistics)
    CardView cardStatistics;
    private BottomSheetBehavior mBehavior;
    private Unbinder unbinder;
    private Activity act;
    private List<JabarModel> jabarModel;
    private JabarAdapter jabarAdapter;
    private AdView mAdView;
    private String ads_id;
    private String ban_id;
    private int ps = 0;
    private int rc = 0;
    private int dt = 0;
    private String od = "";
    private String pd = "";
    private String tgl = "";


    public FragmentBottomJabar(Activity act, List<JabarModel> jabarModel) {
        this.act = act;
        this.jabarModel = jabarModel;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        final View view = View.inflate(getContext(), R.layout.fragment_bottom_jabar, null);
        unbinder = ButterKnife.bind(this, view);
        dialog.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        initData();
        initComponent();
        return dialog;
    }

    private void initData() {
        ads_id = (String) readSP(act, "ads_id", "string");
        ban_id = (String) readSP(act, "ban_id", "string");
        for (JabarModel jb : jabarModel) {
            if (!TextUtils.isEmpty(jb.getTotalOdp())) {
                od = jb.getTotalOdp();
            }
            if (!TextUtils.isEmpty(jb.getTotalPdp())) {
                pd = jb.getTotalPdp();
            }
            if (!TextUtils.isEmpty(jb.getPositif())) {
                ps = jb.getTotalPositifSaatIni();
            }
            if (!TextUtils.isEmpty(jb.getSembuh())) {
                rc = jb.getTotalSembuh();
            }
            if (!TextUtils.isEmpty(jb.getMeninggal())) {
                dt = jb.getTotalMeninggal();
            }
        }
        tgl = jabarModel.get(jabarModel.size() - 1).getTanggal();
    }

    private void initComponent() {
        MobileAds.initialize(act, ads_id);
//        lytSpacer.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels / 2);

        mBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        title.setSelected(true);
        subTitle.setSelected(true);
        title.setText("Jawa Barat");
        subTitle.setText("Last Update : " + tgl);
        odp.setText(od + "");
        pdp.setText(pd + "");
        positive.setText(ps + "");
        recovered.setText(rc + "");
        death.setText(dt + "");
        StartAppSDK.init(act, getString(R.string.startapp), true);
        StartAppAd.disableSplash();
        prepareBan();

        jabarAdapter = new JabarAdapter(act);
        updateData.setAdapter(jabarAdapter);
        updateData.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        DotItemDecoration mItemDecoration = new DotItemDecoration
                .Builder(act)
                .setOrientation(DotItemDecoration.VERTICAL)//if you want a horizontal item decoration,remember to set horizontal orientation to your LayoutManager
                .setItemStyle(DotItemDecoration.STYLE_DRAW)
                .setTopDistance(20)//dp
                .setItemInterVal(10)//dp
                .setItemPaddingLeft(20)//default value equals to item interval value
                .setItemPaddingRight(20)//default value equals to item interval value
                .setDotColor(Color.RED)
                .setDotRadius(2)//dp
                .setDotPaddingTop(0)
                .setDotInItemOrientationCenter(false)//set true if you want the dot align center
                .setLineColor(Color.RED)
                .setLineWidth(1)//dp
                .setEndText("END")
                .setTextColor(Color.RED)
                .setTextSize(10)//sp
                .setDotPaddingText(2)//dp.The distance between the last dot and the end text
                .setBottomDistance(40)//you can add a distance to make bottom line longer
                .create();
        mItemDecoration.setSpanIndexListener(new SpanIndexListener() {
            @Override
            public void onSpanIndexChange(View view, int spanIndex) {
//                view.setBackgroundResource(spanIndex == 0 ? R.drawable.ic_left : R.drawable.ic_right);
            }
        });
        updateData.addItemDecoration(mItemDecoration);

        jabarAdapter.setData(jabarModel);
    }

    private void prepareBan() {
        ads_layout.removeAllViews();
        mAdView = new AdView(act);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(ban_id);
        mAdView.loadAd(new AdRequest.Builder().build());
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                ads_layout.removeAllViews();
                ads_layout.addView(new Banner(act));
            }

            @Override
            public void onAdLoaded() {
                ads_layout.removeAllViews();
                ads_layout.addView(mAdView);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @OnClick({R.id.bt_share, R.id.bt_close, R.id.card_statistics})
    void onViewClicked(View view) {
        ((DashboardActivity) act).showInt();
        switch (view.getId()) {
            case R.id.bt_share:
                String shareBody = "kasus Covid19 di "
                        + "*Jawa Barat*"
                        + "\n\n\uD83D\uDE25 *ODP*: " + od
                        + "\n\uD83D\uDE1F *PDP*: " + pd
                        + "\n\uD83D\uDE14 *Positive*: " + ps
                        + "\n\uD83D\uDE07 *Recovered*: " + rc
                        + "\n\uD83D\uDE2D *Death*: " + dt
                        + "\n\n\uD83D\uDD52 Last Update: " + tgl
                        + "\n\n⬇ Download Apps on : " + readSP(act, "download_uri", "string");
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Jawa Barat");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share with :"));
                break;
            case R.id.bt_close:
                dismiss();
                break;
        }
    }
}