package com.virusinfo.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;
import com.virusinfo.R;
import com.virusinfo.models.CountryModel;
import com.virusinfo.models.ProvinceModel;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.virusinfo.helpers.MyHelper.formatRibuan;
import static com.virusinfo.helpers.SPHelper.createSP;
import static com.virusinfo.helpers.SPHelper.readSP;

public class FragmentBottomDetail extends BottomSheetDialogFragment {

    @BindView(R.id.title)
    AppCompatTextView title;
    @BindView(R.id.subTitle)
    AppCompatTextView subTitle;
    @BindView(R.id.bt_close)
    AppCompatImageButton btClose;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.lyt_parent)
    LinearLayoutCompat lytParent;
    @BindView(R.id.ads)
    LinearLayoutCompat ads_layout;
    @BindView(R.id.positive)
    AppCompatTextView positive;
    @BindView(R.id.card_positive)
    CardView cardPositive;
    @BindView(R.id.recovered)
    AppCompatTextView recovered;
    @BindView(R.id.card_recovered)
    CardView cardRecovered;
    @BindView(R.id.death)
    AppCompatTextView death;
    @BindView(R.id.card_death)
    CardView cardDeath;
    @BindView(R.id.lyt_spacer)
    View lytSpacer;
    @BindView(R.id.bt_share)
    AppCompatImageButton btShare;
    private BottomSheetBehavior mBehavior;
    private Unbinder unbinder;
    private Activity act;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private String namaWilayah;
    private int p;
    private int r;
    private int d;
    private long lastUpdate;
    private boolean isShow_ad = false;

    public FragmentBottomDetail(Activity act, CountryModel countryModel) {
        this.act = act;
        this.namaWilayah = countryModel.getAttributes().getCountryRegion();
        this.p = countryModel.getAttributes().getConfirmed();
        this.r = countryModel.getAttributes().getRecovered();
        this.d = countryModel.getAttributes().getDeaths();
        this.lastUpdate = countryModel.getAttributes().getLastUpdate();
    }

    public FragmentBottomDetail(Activity act, ProvinceModel provinceModel) {
        this.act = act;
        this.namaWilayah = provinceModel.getAttributes().getProvinsi();
        this.p = provinceModel.getAttributes().getKasusPosi();
        this.r = provinceModel.getAttributes().getKasusSemb();
        this.d = provinceModel.getAttributes().getKasusMeni();
        this.lastUpdate = 0;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        final View view = View.inflate(getContext(), R.layout.fragment_bottom_detail, null);
        unbinder = ButterKnife.bind(this, view);
        dialog.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        initComponent();
        return dialog;
    }

    private void initComponent() {
        isShow_ad = (boolean) readSP(act, "isShow_ad", false);
        lytSpacer.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels / 2);

        mBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        btClose.setOnClickListener(v -> {
            showInt();
            dismiss();
        });
        btShare.setOnClickListener(v -> {
            showInt();
            String shareBody = "Covid19 data from *"
                    + namaWilayah
                    + "*\n\n\uD83D\uDE14 *Positive*: " + formatRibuan(p)
                    + "\n\uD83D\uDE07 *Recovered*: " + formatRibuan(r)
                    + "\n\uD83D\uDE2D *Death*: " + formatRibuan(d)
//                    + "\n\n\uD83D\uDD52 Last Update: " + new Date(lastUpdate)
                    + "\n\n⬇ Download Apps on : " + readSP(act, "download_uri", "string");
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, namaWilayah);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share with :"));
        });

        positive.setOnClickListener(v -> showInt());
        recovered.setOnClickListener(v -> showInt());
        death.setOnClickListener(v -> showInt());

        title.setSelected(true);
        subTitle.setSelected(true);
        title.setText(namaWilayah);
        if (lastUpdate > 0) {
            subTitle.setText(new StringBuilder().append("Last Update : ").append(new Date(lastUpdate)));
        }
        positive.setText(new StringBuilder().append(formatRibuan(p)));
        recovered.setText(new StringBuilder().append(formatRibuan(r)));
        death.setText(new StringBuilder().append(formatRibuan(d)));
        if (isShow_ad) {
            MobileAds.initialize(act, (String) readSP(act, "ads_id", "string"));
            prepareBan();
            StartAppSDK.init(act, getString(R.string.startapp), true);
            StartAppAd.disableSplash();
        }
    }

    private void prepareBan() {
        ads_layout.removeAllViews();
        mAdView = new AdView(act);
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId((String) readSP(act, "ban_id", "string"));
        mAdView.loadAd(new AdRequest.Builder().build());
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                ads_layout.removeAllViews();
                ads_layout.addView(new Banner(act));
            }

            @Override
            public void onAdLoaded() {
                ads_layout.removeAllViews();
                ads_layout.addView(mAdView);
            }
        });
    }

    private void prepareInt() {
        createSP(act, "clickCounter", 0);
        mInterstitialAd = new InterstitialAd(act);
        mInterstitialAd.setAdUnitId((String) readSP(act, "int_id", "string"));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                StartAppAd.showAd(act);
            }

            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }
        });
    }

    private void showInt() {
        if (cc() >= 5 && isShow_ad) {
            prepareInt();
        }
    }

    private int cc() {
        int cc = (int) readSP(act, "clickCounter", 0);
        createSP(act, "clickCounter", ++cc);
        return cc;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}