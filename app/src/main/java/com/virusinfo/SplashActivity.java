package com.virusinfo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.ldoublem.loadingviewlib.view.LVGhost;
import com.virusinfo.activities.DashboardActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import dots.animation.textview.TextAndAnimationView;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.LVGhost)
    LVGhost mLVGhost;
    @BindView(R.id.loading)
    TextAndAnimationView loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, DashboardActivity.class));
            finish();
        }, 3000);
        mLVGhost.setViewColor(Color.WHITE);
        mLVGhost.setHandColor(Color.BLACK);
        mLVGhost.startAnim();
    }
}
